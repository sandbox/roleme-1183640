<?php 

/**
 * Ajax callback to check node comment status and update
 */
function janepratt_settatus_ajax(){
	$nid = $_POST['nid'];
	if(!empty($nid)){
		$data = array(
			'nid' => $nid,		
			'status'=>0
	     );
		drupal_write_record('facebook_comments', $data, 'nid');
	}
	print $nid;
}

/**
 * Select nodes to be updated
 */
function update_comment_count(){
	$last_cron_run_time = variable_get('last_cron_run_time',time());
	$time_shift = time()-$last_cron_run_time;
	$offset = 300;
	if($time_shift > $offset){
		$result = db_query_range('SELECT nid FROM {facebook_comments} WHERE status = 0',0,1000);
		$rows = db_affected_rows($result);
		while( $nid = db_result($result)){
			$nodes[$nid] = "'".url('node/'. $nid, array('absolute' => true))."'";
		}
		if(!empty($rows)){
			$response = query_fb_count($nodes);
			$count_iterator = 0;
			foreach($nodes as $nid => $value){
				$data = array(
					'nid' => $nid,
					'fb_like_count'=>$response[$count_iterator]['total_count'],	
					'status'=>1
			     );
			    drupal_write_record('facebook_comments', $data, 'nid');
				$count_data = array(
					'nid' => $nid,		
					'comment_count'=>$response[$count_iterator]['commentsbox_count']
			     );
				drupal_write_record('node_comment_statistics', $count_data, 'nid');
				$count_iterator++;
			}
		}
		variable_set('last_cron_run_time',time());
		$output = $rows. " nodes comment count updated";
	}
	else {
		$output = $offset-$time_shift. " seconds before next comment count update";	
	}
	return $output;
}

/**
 * FBQL query wrapper
 */
function query_fb_count($urls){
	if (!class_exists('Facebook')) {
		$lib_path = module_invoke('libraries', 'get_path', 'facebook-php-sdk');
		$client_include =  $lib_path . '/src/facebook.php';
		include_once($client_include);
	}
	$url_string = implode(',',$urls);
	$facebook = new Facebook(array(
			'appId'  =>  variable_get('fb_application_id', '209995182348925'),
			'secret' => variable_get('fb_application_secret', '9cf9137705fe7c06177fa577061345fb'),
			'cookie' => true, // enable optional cookie support
		));
	try {
		$fql = "SELECT commentsbox_count, total_count FROM link_stat WHERE url IN (".$url_string.")";

		$response = $facebook->api(array(
			'method' => 'fql.query',
			'query' => $fql,
		));
		return $response;
	} catch (FacebookApiException $e) {
		error_log($e);
	}
}