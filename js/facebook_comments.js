Drupal.behaviors.facebook_comments = function (context){
	initFBcommentsCallback();
}

function initFBcommentsCallback(){
	FB.Event.subscribe('comment.create', function(response) {
	 	updateCommentFlag();
	});
	FB.Event.subscribe('comment.remove', function(response) {
	 	updateCommentFlag();
	});
	FB.Event.subscribe('edge.create', function(response) {
	 	updateCommentFlag();
	});
	FB.Event.subscribe('edge.remove', function(response) {
	 	updateCommentFlag();
	});
}


function updateCommentFlag(){
	$.ajax({
		type: 'POST',
		data: {
			'nid': Drupal.settings.jp_current_node_id
		},
		url: Drupal.settings.basePath + 'js/set-status'
	});
}